export const store = {
    state: {
        events: []
    },

    getEventList(){
        return this.state.events;
    },

    getEvent(id){
        return this.state.events.find((event) => event.id == id);
    },

    setUserDetail(id, value){
        var user = this.getUser(id);
        user.detail = value;
    },

    setEvent(event) {
        this.state.events.push(event);
    },

    removeEvent(event) {
      this.state.events.pop(event);
    }
}