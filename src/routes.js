import Main from './components/Main'
import MainWeather from './components/MainWeather'
import MainMusic from './components/MainMusic'
import SavedMusic from './components/SavedMusic'

export const routes = [
    {path: '', component: Main},
    {path: '/weather', component: MainWeather},
    {path: '/music', component: MainMusic},
    {path: '/savedmusic', component: SavedMusic}
]